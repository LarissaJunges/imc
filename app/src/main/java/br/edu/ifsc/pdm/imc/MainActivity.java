package br.edu.ifsc.pdm.imc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
    public void btnCalcular(View view){

        TextView textResulatdo = (TextView) findViewById(R.id.textResultado);
        EditText editPeso = (EditText) findViewById(R.id.editPeso);
        EditText editAltura = (EditText) findViewById(R.id.editAltura);

        int peso = Integer.parseInt(editPeso.getText().toString());
        float altura = Float.parseFloat(editAltura.getText().toString());

        float resultado = peso / (altura * altura);

        if (resultado <= 18.5) {
            textResulatdo.setText("Abaixo do peso!");
        } else if (resultado > 18.5 && resultado <= 24.9) {
            textResulatdo.setText("Peso ideal,Parabéns");
        } else if (resultado > 24.9 && resultado <= 29.9) {
            textResulatdo.setText("Levemente acima do peso");
        } else if (resultado > 30.0 && resultado <= 34.9) {
            textResulatdo.setText("Obesidade grau I");
        } else if (resultado > 35 && resultado <= 39.9) {
            textResulatdo.setText("Obesidade grau II severa");
        } else {
            textResulatdo.setText("Obesidade severa!");
        }
    }
}

